# Payment_Classification

The codes in this repository allows to cluster customer's payments. It will classify each entry in the bank statement into 
categories such as 'Supermarket', 'Petrol', 'Eating Out' etc. It learns from previously classified data, and corrections you 
make when it guesses a category incorrectly, and improves its performance over time.

How to use

Install the required libraries: pip install -r requirements.txt

The img_to_text.py file converts an screen shot of the ANZ bank statement in jpg format to (bank).csv file format.
The example.py file allows to interactively classify the payments and saves the result into AllData.csv file.
The clustering.py file plots a pie chart that illustrates the customer spending behaviour using the information in AllData.csv file.
