from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

Input_file_path = "D:\\Python Code\\Munji\\BankClassify\\AllData.csv"

data = pd.read_csv(Input_file_path, encoding='latin_1', low_memory=False)

Categories = list(set(list(data['cat'])))
cats = np.array(list(data['cat']))
prices = list(data['amount'])

Total_each = []
for i in range(len(Categories)):
    a = np.where(cats==Categories[i])[0]
    Total_each.append(sum(np.array(prices)[a]))

# Plot
labels = Categories
sizes = Total_each

plt.pie(sizes, labels=labels, autopct='%1.1f%%', shadow=True, startangle=140)
plt.axis('equal')
plt.show()


