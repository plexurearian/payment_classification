
import os.path
import pytest
from pytesseract import image_to_string
import pytesseract
import re
import pandas as pd
import numpy as np
from pytesseract import Output
import cv2

Input_file_path = "C:\\Users\\Aryan\\Desktop\\bank.jpg"
Output_file_path = "D:\\Python Code\\Munji\\bank.csv"

pytesseract.pytesseract.tesseract_cmd = r'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'

a = image_to_string(Input_file_path, 'eng')
b = a.split("\n\n")

def strip_list_noempty(mylist):
    newlist = (item.strip() if hasattr(item, 'strip') else item for item in mylist)
    return [item for item in newlist if item != '']
c = strip_list_noempty(b)

img = cv2.imread(Input_file_path)
d = pytesseract.image_to_data(img, output_type=Output.DICT)

x = np.asarray(d['top'])
dic = []
for i in set(x):
    dic.append(sum(x==i))

MAX = max(dic)
Thre = 0.55*MAX
SET = list(set(x))
IDX = []
for i in range(len(dic)):
    if dic[i]>=Thre:
        IDX.append(SET[i])

arr = np.asarray(IDX)
dif = arr[1:]-arr[0:-1]
N_rows = 1
for i in range(len(dif)):
    if abs(dif[i])>1:
        N_rows+=1

out_dic = {}
col_count = 1
values = []
for i in range(len(c)):
    values.append(c[i])
    if len(values)==N_rows:
        out_dic[str(col_count)] = values
        print(values)
        print(len(values))
        values = []
        col_count += 1

def Clean_Data(dic):
    descs = dic['3']
    new_descs = []
    for i in range(len(descs)):
        Temp = re.split('»|\n', descs[i])[0]
        Temp = Temp.rstrip(' v')
        new_descs.append(Temp)

    dic['3'] = new_descs
    amount = dic['4']
    new_amount = []
    for i in range(len(amount)):
        new_amount.append(amount[i][1:])
    dic['4'] = new_amount
    return dic

clean_out_dic = Clean_Data(out_dic)
df = pd.DataFrame(clean_out_dic,columns= list(clean_out_dic))

df.to_csv(Output_file_path, encoding='utf-8', index=False)
